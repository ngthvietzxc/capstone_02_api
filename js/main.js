const product_URL = 'https://63fa2871897af748dcca361f.mockapi.io';

if (localStorage.getItem('cart')) {
    var cart = new Cart();
    cart.list = JSON.parse(localStorage.getItem('cart'));
    document.getElementById('quantityCart').innerHTML = cart.list.length;
} else {
    var cart = new Cart();
}

start();

function start() {
    fetchProducts(renderProducts);
}

function fetchProducts(renderProducts) {
    axios({
        url: `${product_URL}/products`,
        method: 'GET',
    })
        .then((res) => {
            renderProducts(res.data);
        })
        .catch((err) => {
            console.log('🚀 ~ err:', err);
        });
}

document.getElementById('typeProduct').onchange = function () {
    let typeProduct = document.getElementById('typeProduct').value;
    axios({
        url: `${product_URL}/products`,
        method: 'GET',
    })
        .then((res) => {
            switch (typeProduct) {
                case 'Iphone':
                    renderProducts(
                        res.data.filter((product) => product.type === 'Iphone')
                    );
                    break;
                case 'Samsung':
                    renderProducts(
                        res.data.filter((product) => product.type === 'Samsung')
                    );
                    break;
                default:
                    fetchProducts(renderProducts);
                    break;
            }
        })
        .catch((err) => {
            console.log('🚀 ~ err:', err);
        });
};

function addToCart(id) {
    document.getElementById('quantityCart').innerHTML =
        document.getElementById('quantityCart').innerHTML * 1 + 1;

    axios({
        url: `${product_URL}/products/${id}`,
        method: 'GET',
    })
        .then((res) => {
            let cartItem = new CartItem(res.data);
            cart.add(cartItem);
        })
        .catch((err) => {
            console.log('🚀 ~ err:', err);
        });
}

function openCartUI() {
    document.getElementById('bodyUI').innerHTML = `
    <div class="container row mx-5">
                <h2 class="text-light">Giỏ hàng</h2>
                <div class="input-group my-3">
                    <input
                        type="text"
                        class="form-control"
                        placeholder="Tìm sản phẩm"
                        aria-label="Recipient's username"
                        aria-describedby="button-addon2"
                    />
                    <button
                        class="btn btn-outline-light"
                        type="button"
                        id="button-addon2"
                    >
                        Tìm kiếm
                    </button>
                </div>

                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Đơn giá</th>
                            <th>Số lượng</th>
                            <th>Số tiền</th>
                            <th>Thao tác</th>
                        </tr>
                    </thead>
                    <tbody id="cartUI"></tbody>
                </table>
            </div>
    `;
    handleRenderCart(cart.list);
    document.getElementById('showTotalPay').style.display = 'block';
}

function changeQuantity(id, n) {
    cart.list.forEach((e) => {
        if (e.id === id) {
            e.quantity += n;
            if (e.quantity <= 0) {
                xoaCartItem(id);
            }
        }
    });
    handleRenderCart(cart.list);
}

function xoaCartItem(id) {
    cart.list.forEach((e, index) => {
        if (e.id === id) {
            cart.list.splice(index, 1);
        }
    });
    let cartJSON = JSON.stringify(cart);
    //save to Local Storage
    localStorage.setItem('cart', cartJSON);
    handleRenderCart(cart.list);
}

function pay() {
    cart.list.splice(0, cart.list.length);
    localStorage.removeItem('cart');
    handleRenderCart(cart.list);
    document.getElementById('quantityCart').innerHTML = cart.list.length;
}
