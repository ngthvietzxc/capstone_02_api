function renderProducts(productList) {
    let htmlProduct = productList
        .map((e) => {
            return `
        <div class="col mb-5">
                        <div class="card h-100">
                            <img
                                class="card-img-top"
                                src="${e.img}"
                                alt="..."
                            />
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <h5 class="fw-bolder">${e.name}</h5>
                                    ${e.price} $
                                </div>
                            </div>
                            <div
                                class="card-footer p-4 pt-0 border-top-0 bg-transparent"
                            >
                                <div class="text-center">
                                    <a
                                        class="btn btn-outline-dark mt-auto"
                                        href="#"
                                        onclick="addToCart('${e.id}')"
                                        >Add to cart</a
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
        `;
        })
        .join('');
    document.getElementById('productList').innerHTML = htmlProduct;
}

function handleRenderCart(cart) {
    let htmlCart = cart
        .map((e) => {
            return `
            <tr style="height: 100px;">
                <td class="">
                    <div class="d-flex">
                        <img
                            class=""
                            src="${e.img}"
                            alt="..."
                            style="height: 100px; object-fit: contain"
                        />
                        <div class="ms-5">
                            <div class="text-center">
                                <h5 class="fw-bolder">${e.name}</h5>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    ${e.price} $
                </td>
                <td>
                    <div class="d-flex">
                    <button onclick="changeQuantity('${
                        e.id
                    }',-1)" class="border border-light px-1 mx-2">-</button>
                    <span>${e.quantity}</span>
                    <button onclick="changeQuantity('${
                        e.id
                    }',1)" class="border border-light px-1 mx-2">+</button>
                    </div>
                </td>
                <td>
                    ${e.price * e.quantity} $
                </td>
                <td>
                    <button class="btn btn-light" onclick="xoaCartItem('${
                        e.id
                    }')">Xóa</button>
                </td>
            </tr>
            `;
        })
        .join('');
    document.getElementById('cartUI').innerHTML = htmlCart;

    let cartJSON = JSON.stringify(cart);
    //save to Local Storage
    localStorage.setItem('cart', cartJSON);

    //solve Total Pay
    let totalPay = cart.reduce((total, e) => {
        return total + e.price * e.quantity;
    }, 0);
    document.getElementById('totalPay').innerHTML = totalPay;
}
