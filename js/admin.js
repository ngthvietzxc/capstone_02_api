let BASE_URL = 'https://63fa2871897af748dcca361f.mockapi.io';

function fetchProductData() {
    axios({
        url: `${BASE_URL}/products`,
        method: 'GET',
    })
        .then((res) => {
            renderProduct(res.data);
        })
        .catch((err) => {
            console.log('err:', err);
        });
}

fetchProductData();

function addProduct() {
    let product = getInfoForm();
    if (checkValidate(product)) {
        axios({
            url: `${BASE_URL}/products`,
            method: 'POST',
            data: product,
        })
            .then(() => {
                $('#modalForm').modal('hide');
                fetchProductData();
                resetForm();
            })
            .catch((err) => {
                console.log('🚀 ~ err:', err);
            });
    }
}

function deleteProduct(id) {
    axios({
        url: `${BASE_URL}/products/${id}`,
        method: 'DELETE',
    })
        .then(() => {
            fetchProductData();
        })
        .catch((err) => {
            console.log('🚀 ~ err:', err);
        });
}

function modifyProduct(id) {
    axios({
        url: `${BASE_URL}/products/${id}`,
        method: 'GET',
    })
        .then((res) => {
            showInfoToForm(res.data);
        })
        .catch((err) => {
            console.log('🚀 ~ err:', err);
        });
}

function upDateProduct() {
    let newProduct = getInfoForm();
    axios({
        url: `${BASE_URL}/products/${newProduct.id}`,
        method: 'PUT',
        data: newProduct,
    })
        .then((res) => {
            fetchProductData();
            resetForm();
        })
        .catch((err) => {
            console.log('🚀 ~ err:', err);
        });
}

document.getElementById('searchBtn').onclick = () => {
    let searchName = document.getElementById('searchName').value.toLowerCase();
    axios({
        url: `${BASE_URL}/products`,
        method: 'GET',
    })
        .then((res) => {
            let nameList = res.data.filter((e) => {
                return e.name.toLowerCase().includes(searchName);
            });
            renderProduct(nameList);
        })
        .catch((err) => {
            console.log('err:', err);
        });
};

document.querySelector('.fa-chevron-down').style.display = 'none';
let theadPrice = document.getElementById('priceThead');
theadPrice.onclick = () => {
    document
        .querySelector('.fa-chevron-down')
        .classList.toggle('d-inline-block');
    document.querySelector('.fa-chevron-up').classList.toggle('d-none');
    //points.sort(function(a, b){return a - b});

    axios({
        url: `${BASE_URL}/products`,
        method: 'GET',
    })
        .then((res) => {
            console.log(res.data[0].price);
            console.log(res.data[res.data.length - 1].price);
            if (
                document
                    .querySelector('.fa-chevron-up')
                    .classList.contains('d-none')
            ) {
                let decreaseArrayByPrice = sortDecrease(res.data);
                renderProduct(decreaseArrayByPrice);
            } else {
                let decreaseArrayByPrice = sortIncrease(res.data);
                renderProduct(decreaseArrayByPrice);
            }
        })
        .catch((err) => {
            console.log('err:', err);
        });
};

$('#modalForm').on('hidden.bs.modal', function (e) {
    resetForm();
});
