function checkValidate(product) {
    let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
        product;
    let valid = checkEmptyField(name, 'txtName');
    valid &=
        checkEmptyField(price, 'txtPrice') &&
        checkOnlyNumber(price, 'txtPrice');
    valid &= checkEmptyField(screen, 'txtScreen');
    valid &= checkEmptyField(backCamera, 'txtBackCamera');
    valid &= checkEmptyField(frontCamera, 'txtFrontCamera');
    valid &= checkEmptyField(img, 'txtImage') && checkIsLink(img, 'txtImage');
    valid &= checkEmptyField(desc, 'txtDesc');
    valid &= checkEmptyField(type, 'txtType');
    return valid;
}

function checkEmptyField(input, idTag) {
    if (input.length == 0 || input == 0) {
        showErrorValidate(idTag, 'Không được để trống');
        return false;
    } else {
        showErrorValidate(idTag, '');
        return true;
    }
}

function checkOnlyNumber(input, idTag) {
    let re = /^-?\d*\.?\d*$/;
    if (re.test(input)) {
        showErrorValidate(idTag, '');
        return true;
    } else {
        showErrorValidate(idTag, 'Chỉ chứa số');
        return false;
    }
}

function checkIsLink(input, idTag) {
    let re =
        /^(http(s):\/\/.)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/;
    if (re.test(input)) {
        showErrorValidate(idTag, '');
        return true;
    } else {
        showErrorValidate(idTag, 'Link không hợp lệ ("https://")');
        return false;
    }
}
